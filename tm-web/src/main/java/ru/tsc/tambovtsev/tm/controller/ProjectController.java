package ru.tsc.tambovtsev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.service.ProjectService;

import java.util.Collection;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Nullable
    private Collection<Project> getProjects() {
        return projectService.findAll();
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectService.create();
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        projectService.removeById(id);
        return "redirect:/projects";
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("project") final Project project,
            @NotNull final BindingResult result
    ) {
        projectService.save(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String id) {
        @NotNull final Project project = projectService.findById(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", getProjects());
    }

}