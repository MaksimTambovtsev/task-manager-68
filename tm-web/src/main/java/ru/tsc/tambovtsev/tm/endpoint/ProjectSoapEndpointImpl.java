package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectSoapEndpointImpl;
import ru.tsc.tambovtsev.tm.service.ProjectService;
import ru.tsc.tambovtsev.tm.soap.project.*;

import java.util.stream.Collectors;

@Endpoint
public class ProjectSoapEndpointImpl implements IProjectSoapEndpointImpl {

    @NotNull
    public static final String LOCATION_PATH = "/ws";

    @NotNull
    public static final String PORT_NAME = "/ProjectSoapEndpointPort";

    @NotNull
    public static final String NAMESPACE = "http://tsc.ru/tambovtsev/tm/soap/project";

    @NotNull
    @Autowired
    private ProjectService projectService;


    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(@NotNull @RequestPayload final ProjectCreateRequest request) {
        @NotNull final ProjectCreateResponse response = new ProjectCreateResponse();
        response.setProject(projectService.create(request.getName()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@NotNull @RequestPayload final ProjectFindAllRequest request) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProject(
                projectService
                        .findAll()
                        .stream()
                        .collect(Collectors.toList())
        );
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@NotNull @RequestPayload final ProjectFindByIdRequest request) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        response.setProject(projectService.findById(request.getId()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@NotNull @RequestPayload final ProjectSaveRequest request) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@NotNull @RequestPayload final ProjectDeleteRequest request) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(@NotNull @RequestPayload final ProjectDeleteAllRequest request) {
        projectService.remove(request.getProject());
        return new ProjectDeleteAllResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@NotNull @RequestPayload final ProjectClearRequest request) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@NotNull @RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@NotNull @RequestPayload final ProjectCountRequest request) {
        @NotNull final ProjectCountResponse response = new ProjectCountResponse();
        response.setCount(projectService.count());
        return response;
    }
}
