package ru.tsc.tambovtsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @Nullable
    private final String displayName;

    Status(@Nullable String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return NOT_STARTED;
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return NOT_STARTED;
    }

}
