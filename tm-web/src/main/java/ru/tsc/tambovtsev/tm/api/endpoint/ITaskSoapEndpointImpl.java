package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import ru.tsc.tambovtsev.tm.endpoint.TaskSoapEndpointImpl;
import ru.tsc.tambovtsev.tm.soap.task.*;

public interface ITaskSoapEndpointImpl {

    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskCreateResponse create(
            @NotNull
            @RequestPayload TaskCreateRequest request
    );

    @PayloadRoot(localPart = "taskFindAllRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskFindAllResponse findAll(
            @NotNull
            @RequestPayload TaskFindAllRequest request
    );

    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskFindByIdResponse findById(
            @NotNull
            @RequestPayload TaskFindByIdRequest request
    );

    @PayloadRoot(localPart = "taskSaveRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskSaveResponse save(
            @NotNull
            @RequestPayload TaskSaveRequest request
    );

    @PayloadRoot(localPart = "taskDeleteRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskDeleteResponse delete(
            @NotNull
            @RequestPayload TaskDeleteRequest request
    );

    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskDeleteAllResponse clear(
            @NotNull
            @RequestPayload TaskDeleteAllRequest request
    );

    @PayloadRoot(localPart = "taskClearRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskClearResponse clear(
            @NotNull
            @RequestPayload TaskClearRequest request
    );

    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskDeleteByIdResponse deleteById(
            @NotNull
            @RequestPayload TaskDeleteByIdRequest request
    );

    @PayloadRoot(localPart = "taskCountRequest", namespace = TaskSoapEndpointImpl.NAMESPACE)
    TaskCountResponse count(
            @NotNull
            @RequestPayload TaskCountRequest request
    );

}
