package ru.tsc.tambovtsev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.tambovtsev.tm.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {
}
