package ru.tsc.tambovtsev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.tambovtsev.tm.model.Task;

public interface ITaskRepository extends JpaRepository<Task, String> {
}
