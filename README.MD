# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Maksim Tambovtsev

* **E-MAIL**: mtambovtsev@tsconsulting.com

* **E-MAIL**: t1@consulting.com

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* MS Windows 10

## HARDWARE

* **RAM**: 8Gb

* **CPU**: AMD Ryzen 5

* **HDD**: 512Gb

## BUILD APPLICATION

```shell
mvn clean install
```

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
