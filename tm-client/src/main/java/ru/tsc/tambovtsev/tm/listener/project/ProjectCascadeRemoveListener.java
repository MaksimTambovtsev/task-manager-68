package ru.tsc.tambovtsev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class ProjectCascadeRemoveListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-cascade-remove";

    @NotNull
    public static final String DESCRIPTION = "Project cascade remove.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCascadeRemoveListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        @Nullable final String userId = getUserId();
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        //getProjectTaskService().removeProjectById(userId, projectId);
    }

}
