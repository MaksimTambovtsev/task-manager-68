package ru.tsc.tambovtsev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskStartByIdListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getTaskEndpoint().changeTaskStatusById(new TaskChangeStatusByIdRequest(getToken(), id, Status.IN_PROGRESS));
    }

}
