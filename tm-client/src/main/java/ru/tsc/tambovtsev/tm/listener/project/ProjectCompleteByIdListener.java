package ru.tsc.tambovtsev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCompleteByIdListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        @Nullable final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getProjectEndpoint().changeProjectStatusById(new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED));
    }

}
