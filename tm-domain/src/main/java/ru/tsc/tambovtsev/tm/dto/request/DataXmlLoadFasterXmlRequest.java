package ru.tsc.tambovtsev.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataXmlLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
