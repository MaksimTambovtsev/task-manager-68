package ru.tsc.tambovtsev.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveJaxBRequest extends AbstractUserRequest {

    public DataJsonSaveJaxBRequest(@Nullable String token) {
        super(token);
    }

}
