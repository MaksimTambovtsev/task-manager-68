package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

@NoRepositoryBean
public interface IOwnerRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

    M findByUserIdAndId(@Nullable String userId, @Nullable String id);

    List<M> findAllByUserId(@Nullable String userId, Pageable pageable);

    void deleteByUserId(@Nullable String userId);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

}
