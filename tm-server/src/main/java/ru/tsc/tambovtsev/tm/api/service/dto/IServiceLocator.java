package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.service.IDomainService;
import ru.tsc.tambovtsev.tm.api.service.ILoggerService;
import ru.tsc.tambovtsev.tm.api.service.property.IServerPropertyService;
import ru.tsc.tambovtsev.tm.api.service.property.ISessionPropertyService;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskServiceDTO();

    @NotNull
    IProjectService getProjectServiceDTO();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserServiceDTO();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IServerPropertyService getServerPropertyService();

    @NotNull
    ISessionPropertyService getSessionPropertyService();
}
