package ru.tsc.tambovtsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectTaskService;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.dto.model.SessionDTO;
import ru.tsc.tambovtsev.tm.exception.field.NameEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project = projectService.changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectCascadeRemoveResponse cascadeRemoveProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCascadeRemoveRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.removeProjectById(userId, projectId);
        return new ProjectCascadeRemoveResponse();
    }

    @NotNull
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @WebMethod
    public ProjectSizeResponse sizeProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectSizeRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final ProjectSizeResponse response = new ProjectSizeResponse();
        response.setSize(projectService.getSize(userId));
        return response;
    }

    @NotNull
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @Nullable final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectService.create(project);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final SortTable sortTable = request.getSortTableType();
        return new ProjectListResponse(projectService.findAll(userId, sortTable));
    }

    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        projectService.removeById(userId, id);
        return new ProjectRemoveByIdResponse(null);
    }

    @NotNull
    @WebMethod
    public ProjectShowByIdResponse showProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = projectService.findById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        final ProjectDTO project = projectService.findById(userId, id);
        if (project == null) return new ProjectUpdateByIdResponse(null);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectService.create(project);
        return new ProjectUpdateByIdResponse(project);
    }

}
