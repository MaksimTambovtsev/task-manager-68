package ru.tsc.tambovtsev.tm.api.service.property;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.component.ISaltProvider;

public interface IApplicationPropertyService{

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
